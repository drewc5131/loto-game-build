import hashlib
import os
jsonString = '{\n"data": ['
rootDir = 'P:\Epic Games\Projects\LOTOGame\Build\EarlyPrototype'
for dirName, subdirList, fileList in os.walk(rootDir):
    for fname in fileList:
        if not dirName.startswith("P:\\Epic Games\\Projects\\LOTOGame\\Build\\EarlyPrototype\\WindowsNoEditor\\LOTOGame\\Saved") and not dirName.startswith("P:\\Epic Games\\Projects\\LOTOGame\\Build\\EarlyPrototype\\.git"):
            filename = "%s\\%s"%(dirName.replace("P:\\Epic Games\\Projects\\LOTOGame\\Build\\EarlyPrototype\\WindowsNoEditor\\" ,""), fname)
            if filename not in ["P:\\Epic Games\\Projects\\LOTOGame\\Build\\EarlyPrototype\\.gitignore", "P:\\Epic Games\\Projects\\LOTOGame\\Build\\EarlyPrototype\\lotomanifest.json", "P:\\Epic Games\\Projects\\LOTOGame\\Build\\EarlyPrototype\\makemanifest.py"]:
                jsonString = jsonString + '{"Filename": "%s", "sha256" : "%s"},\n' %(filename, os.stat("%s" %"%s\\%s" %(dirName, fname)).st_size)
    
jsonString = jsonString[:-2] + '\n]}'
jsonString = jsonString.replace("\\", "/").replace("P:/Epic Games/Projects/LOTOGame/Build/EarlyPrototype/WindowsNoEditor/", "")
print(jsonString)
file = open("lotomanifest.json", "w")
file.write(jsonString)